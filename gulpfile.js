var gulp = require('gulp'),
  compass = require('gulp-compass'),
	csslint = require('gulp-csslint'),
	uglify = require('gulp-uglify'),
	concat = require('gulp-concat'),
  minifyCSS = require('gulp-minify-css');

var styleDir = ['./sass/*/*.scss', './sass/*.scss'];

gulp.task('compass', function() {
  gulp.src(styleDir)
    .pipe(compass({
      config_file: './config.rb',
      css: 'css',
      sass: 'sass',
      require: ['sass-globbing']
    }))
    .pipe(minifyCSS())
    .pipe(gulp.dest('./css/min'));
});

gulp.task('default', function() {
	gulp.start('compass');
  gulp.watch(styleDir, ['compass']);
});
